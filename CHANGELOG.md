## [1.1.0](https://gitlab.com/99protsenti/prettier-config/compare/v1.0.0...v1.1.0) (2024-12-02)

### Features

* add support for node 22 ([450f75b](https://gitlab.com/99protsenti/prettier-config/commit/450f75b50ebfc9ebdcb688d2090ec06a6d114bc0))

## 1.0.0 (2023-10-11)


### Features

* create prettier configuration ([53ff446](https://gitlab.com/99protsenti/prettier-config/commit/53ff446f30719c96fe77a9dd9ccc059490c2d865))
