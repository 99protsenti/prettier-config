module.exports = {
  arrowParens: "avoid",
  bracketSpacing: false,
  trailingComma: "all",
  tabWidth: 2,
  semi: false,
  quoteProps: "consistent",
  importOrder: ["^[./]"],
  importOrderSeparation: true,
  importOrderSortSpecifiers: true,
  plugins: ["@trivago/prettier-plugin-sort-imports"],
  overrides: [],
}
