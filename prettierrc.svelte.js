const baseConfig = require("@99protsenti/prettier-config")

module.exports = {
  ...baseConfig,
  plugins: [...baseConfig.plugins, "prettier-plugin-svelte"],
}
